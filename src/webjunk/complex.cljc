(ns webjunk.complex
  (:refer-clojure :exclude [+ - * / ])
  (:require [clojure.pprint :as pprint :refer [cl-format]]
            [webjunk.generic-math :refer
             [root-type + - * /
              abs exp conjugate sin cos log sgn pow sqr sqrt tan
              asin acos atan sinh cosh tanh asinh acosh atanh]]
            [clojure.test :as test :refer [is]])
  #?(:clj (:import [clojure.lang Indexed IFn])))

#?(:cljs (def Number js/Number))

(declare ensure-complex)

(deftype Complex [real imag]
  #?(:clj Object :cljs IEquiv)
  (#?(:clj equals :cljs -equiv) [_ y]
    (let [y (ensure-complex y)]
      (== real (.-real y))
      (== imag (.-imag y))))
  #?(:clj Indexed :cljs IIndexed)
  (#?(:clj nth :cljs -nth) [_ n default]
    (case n
      0 real
      1 imag
      default))
  (#?(:clj nth :cljs -nth)
    [this n]
    (or (nth this n nil)
        (throw (ex-info
                "Complex numbers can only be indexed by 0 or 1." {}
                :index-out-of-bounds))))
  IFn
  (#?(:clj invoke :cljs -invoke) [z n] (nth z n)))
#?(:cljs (derive Complex root-type))


(defn complex
  ([]    (Complex. 0 0))
  ([x]   (Complex. x 0))
  ([x y] (Complex. x y)))

(defn strictly-complex? [z] (= (type z) Complex))

(defmulti ensure-complex type)
(defmethod ensure-complex :default [z]
  (if (strictly-complex? z) z
      (complex z)))

(defmulti complex? type)
(defmethod complex? :default [z] (or (strictly-complex? z) (number? z)))

(def i (Complex. 0 1))
(defn real [[x _]] x)
(defn imag [[_ y]] y)


#?(:clj
   (do (defmethod print-method Complex [[x y] w]
         (cl-format w "~a~a~ai" x (if (not (neg? y)) "+" "") y))

       (defmethod print-dup Complex [[x y] w]
         (.write w (str "#" `complex [x y])))))


(defmethod + [Complex Complex]
  [[x0 y0] [x1 y1]]
  (complex (+ x0 x1) (+ y0 y1)))
(is (= (+ (complex 1 1) (complex 1 3)) (complex 2.0 4.0)))
(defmethod + [Complex Number] [z k] (+ z (ensure-complex k)))
(defmethod + [Number Complex] [k z] (+ z k))

(defmethod - Complex [[x y]] (complex (- x) (- y)))

(defmethod * [Complex Complex]
  [[x0 y0] [x1 y1]]
  (complex (- (* x0 x1) (* y0 y1))
           (+ (* x0 y1) (* x1 y0))))
(defmethod * [Complex Number] [z k] (* z (ensure-complex k)))
(defmethod * [Number Complex] [k z] (* z k))

(defmethod conjugate Complex [[x y]] (complex x (- y)))

(defn sqr-abs [[x y]] (+ (sqr x) (sqr y)))
(defmethod abs Complex [z] (sqrt (sqr-abs z)))

(defmethod / Complex [z] (* (conjugate z) (/ (sqr-abs z))))
(defmethod / [Complex Number] [z k] (* z (/ k)))
(defmethod / [Number Complex] [k z] (* k (/ z)))


(defn cis [t]
  (+ (cos t)
     (* i (sin t))))

(defmethod exp Complex [[x y]] (* (exp x) (cis y)))
(defn Arg [[x y]] (Math/atan2 y x))
(defn polarize [z] [(abs z) (Arg z)])

(defmethod log Complex [z]
  (let [[r theta] (polarize z)]
    (complex (log r) theta)))

(defmethod sgn Complex [z] (/ z (abs z)))


(defmethod cos  Complex [z] (cosh (* i z)))
(defmethod sin  Complex [z] (/ (sinh (* i z)) i))
(defmethod tan  Complex [z] (/ (sinh (* i z))
                               (cosh (* i z))
                               i))
(defmethod acos Complex [z] (/ (acosh z) i))
(defmethod asin Complex [z] (/ (asinh (* i z)) i))
(defmethod atan Complex [z] (/ (atanh (* i z)) i))
